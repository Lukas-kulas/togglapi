"""
Toggl api-wrapper
"""
import requests
import os
import json


class EndPoints():
    """Class stores endpoints strings"""
    ROOT = "https://www.toggl.com/api/v8/"
    WORKSPACES = ROOT + "workspaces"
    CLIENTS = ROOT + "clients"
    TAGS = ROOT + "tags"
    PROJECTS = ROOT + "projects"
    TASKS = ROOT + "tasks"

    TIME = ROOT + "time_entries/"
    START_TIME = TIME + "start"
    CURRENT_RUNNING_TIME = TIME + "current"

    @staticmethod
    def WORKSPACE_DETAIL(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid))

    @staticmethod
    def WORKSPACE_PROJECTS(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid), "projects")

    @staticmethod
    def WORKSPACE_USERS(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid), "users")

    @staticmethod
    def WORKSPACE_CLIENTS(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid), "clients")

    @staticmethod
    def WORKSPACE_TASK(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid), "tasks")

    @staticmethod
    def WORKSPACE_TAGS(wid):
        return os.path.join(EndPoints.WORKSPACES, str(wid), "tags")

    @staticmethod
    def PROJECT_DETAIL(pid):
        return os.path.join(EndPoints.PROJECTS, str(pid))

    @staticmethod
    def TASK_DETAIL(id):
        return os.path.join(EndPoints.TASKS, str(id))

    @staticmethod
    def TAG_DETAIL(id):
        return os.path.join(EndPoints.TAGS, str(id))

    @staticmethod
    def PROJECT_USERS(pid):
        return os.path.join(EndPoints.PROJECTS, str(pid), "project_users")

    @staticmethod
    def PROJECT_TASKS(pid):
        return os.path.join(EndPoints.PROJECTS, str(pid), "tasks")

    @staticmethod
    def STOP_TIME(id):
        return os.path.join(EndPoints.PROJECTS, str(id), "stop")


class Toggl():
    """Methods for retriving data from endpoints"""

    def __init__(self):
        self.head = {
            "Authorization": "Dook",
            "Content-Type": "application/json",
            "Accept": "*/*",
            "User-Agent": "python",
        }

    def set_api_key(self, api_key):
        authHeader = api_key + ":" + "api_token"
        authHeader = "Basic " + authHeader.encode("base64").rstrip()
        self.head['Authorization'] = authHeader

    @staticmethod
    def check_data(r):
        if r.status_code == requests.codes.ok:
            return r.status_code
        else:
            return "Error: " + str(r.status_code)

    # ------------------------------
    # Methods for managing Workplace
    # ------------------------------
    def get_all_workplaces(self):
        """Get data about all the workspaces where the token owner belongs"""
        r = requests.get(EndPoints.WORKSPACES, headers=self.head)
        return self.check_data(r)

    def get_workplace(self, wid):
        """Get info about single workspace"""
        r = requests.get(EndPoints.WORKSPACE_DETAIL(wid), headers=self.head)
        return self.check_data(r)

    def get_workplace_projects(self, wid):
        """Get data about workspace projects - token owner must be admin"""
        r = requests.get(EndPoints.WORKSPACE_PROJECTS(wid), headers=self.head)
        return self.check_data(r)

    def get_workplace_users(self, wid):
        """Get data about workspace users - token owner must be admin"""
        r = requests.get(EndPoints.WORKSPACE_USERS(wid), headers=self.head)
        return self.check_data(r)

    def get_workplace_clients(self, wid):
        """Get data about workspace clients - token owner must be admin"""
        r = requests.get(EndPoints.WORKSPACE_CLIENTS(wid), headers=self.head)
        return self.check_data(r)

    def get_workplace_tasks(self, wid):
        """Get data about workspace tasks - token owner must be admin"""
        r = requests.get(EndPoints.WORKSPACE_TASK(wid), headers=self.head)
        return self.check_data(r)

    def get_workplace_tags(self, wid):
        """Get data about workspace tags"""
        r = requests.get(EndPoints.WORKSPACE_TAGS(wid), headers=self.head)
        return self.check_data(r)

    # -----------------------------
    # Methods for managing Projects
    # -----------------------------
    def get_project(self, pid):
        """Create new Project"""
        r = requests.get(EndPoints.PROJECT_DETAIL(pid), headers=self.head)
        return self.check_data(r)

    def get_project_users(self, pid):
        """Get info about user in Project"""
        r = requests.get(EndPoints.PROJECT_USERS(pid), headers=self.head)
        return self.check_data(r)

    def get_project_tasks(self, pid):
        """Get info about tasks in project"""
        r = requests.get(EndPoints.PROJECT_TASKS(pid), headers=self.head)
        return self.check_data(r)

    # --------------------------
    # Methods for managing Tasks
    # --------------------------
    def create_task(self, name, pid):
        """Create task in project"""
        params = {
            "task": {
            "name": name,
            "pid": pid
            }
        }
        data = json.JSONEncoder().encode(params)
        r = requests.post(EndPoints.TASKS, headers=self.head, data=data)
        return self.check_data(r)

    def get_task(self, task_id):
        """Get info about task"""
        r = requests.get(EndPoints.TASK_DETAIL(task_id), headers=self.head)
        return self.check_data(r)

    def update_task(self, task_id, is_active=None, estimate=None, name=None):
        """Update task details"""
        params = {
            "task": {
                "active": is_active,
                "estimated_seconds": estimate,
                "name": name
            }
        }
        data = json.JSONEncoder().encode(params)
        r = requests.put(EndPoints.TASK_DETAIL(task_id), headers=self.head,
                         data=data)
        return self.check_data(r)

    def delete_task(self, task_id):
        """Delete task"""
        r = requests.delete(EndPoints.TASK_DETAIL(task_id), headers=self.head)
        return self.check_data(r)

    # -------------------------
    # Methods for managing Tags
    # -------------------------
    def create_tag(self, wid, name, ):
        """Create new tag in workspace"""
        params = {
            "tag": {
            "name": name,
            "wid": wid
            }
        }
        data = json.JSONEncoder().encode(params)
        r = requests.post(EndPoints.TAGS, headers=self.head, data=data)

        return self.check_data(r)

    def update_tag(self, tag_id, name):
        """Update tag details"""
        params = {
            "tag": {
            "name": name
            }
        }
        data = json.JSONEncoder().encode(params)
        r = requests.put(EndPoints.TAG_DETAIL(tag_id), headers=self.head,
                         data=data)
        return self.check_data(r)

    def delete_tag(self, tag_id):
        """Delete tag"""
        r = requests.delete(EndPoints.TAG_DETAIL(tag_id), headers=self.head)
        return self.check_data(r)

    # -----------------------------
    # Methods for managing Time
    # -----------------------------
    def start_time_entry(self, pid, description, creator):
        """Stat timer in project"""
        params = {
            "time_entry": {
            "description": description,
            "pid": pid,
            "created_with": creator
            }
        }
        data = json.JSONEncoder().encode(params)

        r = requests.post(EndPoints.START_TIME, headers=self.head, data=data)
        return self.check_data(r)

    def current_running_time(self):
        """Data about current running timer"""
        r = requests.get(EndPoints.CURRENT_RUNNING_TIME, headers=self.head)
        return self.check_data(r)

    def stop_time_entry(self, entry_id):
        """Stop time couter"""
        r = requests.put(EndPoints.STOP_TIME(entry_id), headers=self.head)
        return self.check_data(r)
