"""
Toggl api-wrapper testing class
"""
import unittest
import requests

from mock import Mock, patch
from Togglapi import Toggl, EndPoints


class TestCalls(unittest.TestCase):
    toggl = Toggl()

    # Testowe konto 636beae90e7893553a16e83f74978705
    toggl.set_api_key("636beae90e7893553a16e83f74978705")

    endpoint = EndPoints()

    # --------------------------
    # Endpoints Tests
    # --------------------------
    def test_WORKSPACE_DETAIL(self):
        assert self.endpoint.WORKSPACE_DETAIL(12) == \
               "https://www.toggl.com/api/v8/workspaces/12"

    def test_WORKSPACE_PROJECTS(self):
        assert self.endpoint.WORKSPACE_PROJECTS(12) == \
               "https://www.toggl.com/api/v8/workspaces/12/projects"

    def test_WORKSPACE_USERS(self):
        assert self.endpoint.WORKSPACE_USERS(12) == \
               "https://www.toggl.com/api/v8/workspaces/12/users"

    def test_WORKSPACE_CLIENT(self):
        assert self.endpoint.WORKSPACE_CLIENTS(12) == \
               "https://www.toggl.com/api/v8/workspaces/12/clients"

    def test_WORKSPACE_TASK(self):
        assert self.endpoint.WORKSPACE_TASK(12) == \
               "https://www.toggl.com/api/v8/workspaces/12/tasks"

    def test_WORKSPACE_TAGS(self):
        assert self.endpoint.WORKSPACE_TAGS(12) == \
               "https://www.toggl.com/api/v8/workspaces/12/tags"

    def test_PROJECT_DETAIL(self):
        assert self.endpoint.PROJECT_DETAIL(12) == \
               "https://www.toggl.com/api/v8/projects/12"

    def test_TASK_DETAIL(self):
        assert self.endpoint.TASK_DETAIL(12) == \
               "https://www.toggl.com/api/v8/tasks/12"

    def test_TAG_DETAIL(self):
        assert self.endpoint.TAG_DETAIL(12) == \
               "https://www.toggl.com/api/v8/tags/12"

    def test_PROJECT_USERS(self):
        assert self.endpoint.PROJECT_USERS(12) == \
               "https://www.toggl.com/api/v8/projects/12/project_users"

    def test_PROJECT_TASKS(self):
        assert self.endpoint.PROJECT_TASKS(12) == \
               "https://www.toggl.com/api/v8/projects/12/tasks"

    def test_STOP_TIME(self):
        assert self.endpoint.STOP_TIME(12) == \
               "https://www.toggl.com/api/v8/projects/12/stop"

    # --------------------------
    # API-wrapper Tests
    # --------------------------

    # Projects
    def test_getProject(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_project(123456) == 200

    def test_getProject_fail(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 400
            assert self.toggl.get_project(1456101532323423) == "Error: 400"

    def test_getProjectUsers(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_project_users(123456) == 200

    def test_getProjectUsers_fail(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 404
            assert self.toggl.get_project_users(1456101532323423) == "Error: 404"

    def test_getProjectTasks(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_project_users(123456) == 200

    def test_getProjectTasks_fail(self):
        assert self.toggl.get_project_users(1456101532323423) == "Error: 400"

    # Time
    def test_startTimeEntry(self):
        with patch.object(requests, 'post') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.start_time_entry(14542758, "Test", "User") == 200

    def test_currentRunningTime(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.current_running_time() == 200

    def test_stopTimeEntry(self):
        with patch.object(requests, 'put') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.stop_time_entry(123456) == 200

    # Tag
    def test_createTag_fail(self):
        with patch.object(requests, 'post') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.create_tag(1382465, "Test") == 200

    def test_updateTag_fail(self):
        with patch.object(requests, 'put') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.update_tag(1382465, "Test") == 200

    def test_deleteTag_fail(self):
        with patch.object(requests, 'delete') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.delete_tag(1456101532323423) == 200

    # Task
    def test_createTask(self):
        with patch.object(requests, 'post') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.create_task(1382465, "Test") == 200

    def test_getTask(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_task(1382465) == 200

    def test_updateTask(self):
        with patch.object(requests, 'put') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.update_task(12345, "true", "12:32", "Test") == 200

    def test_deleteTask_fail(self):
        with patch.object(requests, 'delete') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.delete_task(1382465) == 200

    # Workspace
    def test_getAllWorkplaces(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_all_workplaces() == 200

    def test_getWorkplace(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace(1382465) == 200

    def test_etWorkplaceClients(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace_clients(1382465) == 200

    def test_getWorkplaceUsers(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace_users(1382465) == 200

    def test_getWorkplaceProjects(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace_projects(1382465) == 200

    def test_getWorkplaceTasks(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace_tasks(1382465) == 200

    def test_getWorkplaceTags(self):
        with patch.object(requests, 'get') as get_mock:
            get_mock.return_value = mock_response = Mock()
            mock_response.status_code = 200
            assert self.toggl.get_workplace_tags(1382465) == 200
